[Adblock Plus 3.14.2]
! =====================
! This file removes all ads and popups on fandom.com sites, including subdomains.
! =====================
! _______________
! Ban by class attribute
fandom.com##.global-navigation
fandom.com##.global-footer
fandom.com##.bottom-ads-container
! _______________
! Ban by ID
fandom.com###articleComments
fandom.com###mixed-content-footer
fandom.com###highlight__main-container
